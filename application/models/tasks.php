<?php

function tasks($link){
    
    $query = "SELECT * FROM nc ORDER BY id DESC";
    $result = mysqli_query($link,$query);
    
    if (!$result)
        die(mysqli_error($link));
    
    $n = mysqli_num_rows($result);
    $tasks = array();
    
    for ($i = 0; $i < $n; $i++){
        $row = mysqli_fetch_assoc($result);
        $tasks[] = $row;
    }
    return $tasks;
}
function articles_get($link, $id){
    $query = sprintf("SELECT * FROM nc WHERE id = %d", (int)$id);
    $result = mysqli_query($link, $query);
    
    if (!$result)
        die(mysqli_error($link));
    
    $article = mysqli_fetch_assoc($result);
    
    return $article;
    
}
function articles_new($link, $title, $content){
    
    $title = trim($title);
    $content = trim($content);
    
    if ($title == '')
        return false;
    
    $t = "INSERT INTO nc (title, content) VALUES ('%s','%s')";
    $query = sprintf($t, mysqli_real_escape_string($link, $title),mysqli_real_escape_string($link, $content));
    
    echo $query;
    $result = mysqli_query($link, $query);
    
    if (!$result)
        die(mysqli_error($link));
    return true;
    
    
}
function articles_edit($link, $id, $title, $content){
    $title = trim($title);
    $content = trim($content);
    $id = (int)$id;
    
   
    $sql = "UPDATE nc SET content='%s' WHERE id='%d'";
    
    $query = sprintf($sql, mysqli_real_escape_string($link, $content),$id);
    $result = mysqli_query($link, $query);
    
    if (!$result)
        die(mysqli_error($link));
    
    return mysqli_affected_rows($link);
    
}
function articles_delete($link, $id)
{
    $id = (int)$id;
    
    if ($id == 0)
        return false;
    
    $query = sprintf("DELETE FROM nc WHERE id='%d'", $id);
    $result = mysqli_query($link, $query);
    
    if(!$result)
        die(mysqli_error($link));
    
    return mysqli_affected_rows($link);
    
}

?>