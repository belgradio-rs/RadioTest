<?php
require_once 'TaskEntity.php';
class TaskModel extends Model
{
    private array $tasks = [];
    private $tegs;
    private $statuses;
    private $task_teg;
    private $task_status;
    

    public function getData(){
        return $this->tasks;
    }
    
	public function saveData($arr)
	{	
        $this->setData($arr);        
        $this->processTegsList();
        $this->processStatusList();   
		        
	}

    private function processTegsList(){        
        foreach($this->task_teg as $key=>$teg ){

            $taskIndex = $this->indexOfTask($teg['task_id']);
            $tegIndex = $this->indexOfTeg($teg['teg_id']);
            if($taskIndex != -1 && $tegIndex!= -1){
                $task = new Task( $this->tasks[$taskIndex]);
                $task->addTeg($this->tegs[$tegIndex]['name']);
                $this->tasks[$key] = $task;
            }
        }

        
    }

    private function processStatusList(){
        foreach ($this->task_status as $key=>$value) { 
            
            $taskIndex = $this->indexOfTask($value['status_id']);
            $statusIndex = $this->indexOfStatus($value['teg_id']);
            if($taskIndex != -1 && $statusIndex!= -1){
                $task = new Task( $this->tasks[$taskIndex]);
                $task->setStatus($this->tegs[$statusIndex]['name']);
                $this->tasks[$key] = $task;
            }
        }  
    }

    private function indexOfTask($id){


        foreach($this->tasks as $key=>$task) { 
            if($task['id']==$id){
                return $key;
            }          
        } 
        return -1;
    }
    private function indexOfTeg($id){
        foreach ($this->tegs as $key=>$teg) { 
            if($teg['id']==$id){
                return $key;
            }          
        } 
        return -1;
    }
    private function indexOfStatus($id){
        foreach ($this->statuses as $key=>$status) { 
            if($status['id']==$id){
                return $key;
            }          
        } 
        return -1;
    }

    private function setData($arr){

        foreach($arr['tasks'] as $task){            
            $this->tasks =   new Task($task);            
        }        
        $this->tegs  =  $arr['tegs'];
        $this->task_teg=  $arr['task_teg'];
        $this->statuses=  $arr['statuses'];
        $this->task_sstatus=  $arr['task_status'];

    }


}